#include "animationController.h"
#include <SFML\Graphics.hpp>
#include <cassert>

AnimationController::AnimationController()
	: sprite(nullptr)
	, textureManager(nullptr)
	, currentAnim(nullptr)
	, currentAnimID(-1)
	, currentFrameIdx(0)
	, frameElapsed(0.0f)
{

}

AnimationController::~AnimationController()
{
	clear();
	sprite = nullptr;
}

void AnimationController::init(sf::Sprite* sprite, TextureManager* textureManager)
{
	this->sprite = sprite;
	this->textureManager = textureManager;
}

void AnimationController::add(AnimID id, const AnimEntry& entry)
{
	animationTable[id] = entry;
}

void AnimationController::change(AnimID id)
{
	currentAnimID = id;
	assert(animationTable.find(id) != animationTable.end() && "AnimationController::change - ID NOT FOUND");
	assert(sprite != nullptr && "AnimationController::change - SPRITE IS NULL");

	currentAnim = &animationTable[currentAnimID];
	currentFrameIdx = 0;
	frameElapsed = currentAnim->frames[currentFrameIdx].delay;
	sprite->setTexture(*textureManager->getTexture(currentAnim->frames[currentFrameIdx].texID));
	sprite->setTextureRect(currentAnim->frames[currentFrameIdx].textureRect);
	sprite->setColor(sf::Color::White);
}

void AnimationController::update(float dt)
{
	if (currentFrameIdx == currentAnim->numFrames) return; // TODO: Handle looping.
	
	frameElapsed -= dt;
	if (frameElapsed <= 0)
	{
		frameElapsed = 0.0f;
		currentFrameIdx++;
		if (currentFrameIdx == currentAnim->numFrames)
		{
			if (currentAnim->looping)
			{
				currentFrameIdx = 0;
			}
			else return;
		}
		frameElapsed = currentAnim->frames[currentFrameIdx].delay;
		sprite->setTexture(*textureManager->getTexture(currentAnim->frames[currentFrameIdx].texID));
		sprite->setTextureRect(currentAnim->frames[currentFrameIdx].textureRect);
		sprite->setColor(sf::Color::White);
	}
}

void AnimationController::clear()
{

}

bool AnimationController::isFinished() const
{
	return (currentFrameIdx == currentAnim->numFrames);
}

