#ifndef ANIM_CONTROLLER_H
#define ANIM_CONTROLLER_H

#include <functional>
#include <vector>
#include <map>
#include <SFML/Graphics.hpp>
#include "textureManager.h"

typedef int AnimID;

struct FrameEntry
{
	TextureId texID;
	sf::IntRect textureRect;
	float delay;
};

struct AnimDataEntry
{
	bool looping;
	std::vector<FrameEntry> frames;
};
struct AnimData
{
	std::vector<AnimID> ids;
	std::vector<AnimDataEntry> entries;
	AnimID startAnim;
};

struct AnimEntry
{
	AnimID id;
	int numFrames;
	std::vector<FrameEntry> frames;
	bool looping;
};

//typedef std::function<void()> EndAnimationCallback;
//typedef std::function<void()> StartAnimationCallback;
//typedef std::function<void(int)> FrameAnimationCallback;
//
//struct AnimationCallback
//{
//	AnimID id;
//	std::vector<EndAnimationCallback> endAnimationListeners;
//	std::vector<EndAnimationCallback> startAnimationListeners;
//	std::vector<std::pair<int, FrameAnimationCallback>> frameAnimationListeners;
//};

class AnimationController
{
public:
	void init(sf::Sprite* sprite, TextureManager* textureManager);
	void add(AnimID id, const AnimEntry& entry);
	void change(AnimID id);
	void update(float dt);
	void clear();
	bool isFinished() const;

	AnimID getCurrentAnimID() const { return currentAnimID; }

	AnimationController();
	~AnimationController();
	
private:	
	std::map<AnimID, AnimEntry> animationTable;

	AnimEntry* currentAnim;
	AnimID currentAnimID;
	int currentFrameIdx;
	float frameElapsed;

	TextureManager* textureManager;
	sf::Sprite* sprite;
};

#endif