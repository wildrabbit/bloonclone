#include "behaviours.h"

#include <iostream>

#include <cmath>
#include <random>
#include "input.h"

#include "entity.h"

static const float DEAD_ZONE = 0.25f;
static const float ACCEL = 300.0f;


static std::random_device rd;
static std::mt19937 mt(rd());
static std::default_random_engine generator;
static std::uniform_real_distribution<float> rd_dist(-0.1f, 0.3f);
static std::uniform_real_distribution<float> rd_move(0.0f, 1.0f);
static std::uniform_real_distribution<float> rd_moveDist(-1.5f, 2.f);
static std::uniform_int_distribution<int> rd_dir(0, 1);

PlayerBehaviour::PlayerBehaviour(Character* chara, Input* input)
	: CharaBehaviour(chara)
	, input(input)
	, state(State::Playing)
	, HP(maxHP)
	, lives(maxLives)
{}

bool PlayerBehaviour::alive()
{
	return state != State::Dead;
}

void PlayerBehaviour::changeState(PlayerBehaviour::State next, bool force)
{
	if (next == state && !force) return;
	switch (next)
	{
	case PlayerBehaviour::State::Playing:
	{
		chara->acceleration = ACCEL;
		break;
	}
	case PlayerBehaviour::State::Dying:
	{
		chara->acceleration = 0.0f;
		break;
	}
	case PlayerBehaviour::State::Dead:
	{
		deadElapsed = deadRecoveryDelay;
		chara->acceleration = 0.0f;
		break;
	}
	default:
		break;
	}
	state = next;
}

void PlayerBehaviour::resolveInput(float dt)
{
	if (disableInputElapsed >= 0)
	{
		disableInputElapsed -= dt;
	}
	else
	{
		if (fabs(input->xAxis) < DEAD_ZONE)
		{
			input->xAxis = 0.0f;
		}
		x = input->xAxis;
	}
	jumpRequest = state == State::Playing && input->jumpJustPressed;
}

void AIBehaviour::changeState(State nextState, bool force)
{
	if (nextState == state && !force) return;
	std::cout << "Changing enemy state from " << static_cast<int>(state) << " to " << static_cast<int>(nextState) << std::endl;


	if (nextState == State::Playing)
	{
		chara->acceleration = ACCEL;
	}
	else
	{
		if (nextState == State::Falling)
		{
			collisionsElapsed = collisionGraceDelay;
			fallStartPosition = chara->sprite.getPosition();
			fallTime = 0.0f;
		}
		x = 0;
		chara->acceleration = 0.0f;
		chara->velocity = { 0.0f, 0.0f };
	}
	collisionsDisabled = nextState == State::Falling;
	state = nextState;
}

void AIBehaviour::resolveInput(float dt)
{
	jumpRequest = false;

	switch (state)
	{
		case State::Dead:
		{
			x = 0.0f;
			break;
		}
		case State::Dying:
		{
			x = 0.0f;
			break;
		}
		case State::Falling:
		{
			x = sinf(dt);
			chara->acceleration = 0;			
			break;
		}
		case State::Inflating:
		{
			x = 0;
			chara->acceleration = 0;
			break;
		}
		case State::Playing:
		{
			if (jumpDelayElapsed < 0)
			{
				jumpDelayElapsed = jumpDecisionDelay + rd_dist(mt);
				jumpRequest = rd_move(mt) > 0.35f; //rd_dir(mt) != 0;
			}
			else
			{
				jumpDelayElapsed -= dt;
			}

			if (moveElapsed < 0)
			{
				moveElapsed = moveDecisionDelay + rd_moveDist(mt);

				if (rd_move(mt) < moveXChance)
				{
					if (rd_dir(mt))
					{
						x = -1.f;
					}
					else
					{
						x = 1.f;
					}
				}
				else
				{
					x = 0.0f;
				}
			}
			else
			{
				moveElapsed -= dt;
			}
			break;
		}
	}
	
}

bool AIBehaviour::alive()
{
	return state != State::Dead;
}

//--------------------------------------------
void PlayerBehaviour::updateLogic(float dt, const sf::Vector2f& screenSize, sf::Vector2f& velocity, sf::Vector2f& position)
{
	if (state == State::Dying  && chara->sprite.getPosition().y > screenSize.y + chara->size.y )
	{
		changeState(State::Dead);		
	}
	else if (state == State::Dead)
	{
		if (deadElapsed < 0)
		{
			velocity = { 0, 0 };
			position = chara->startPosition;
			changeState(State::Playing);
		}
		else
		{
			deadElapsed -= dt;
		}
	}
	else
	{
		velocity = chara->defaultLocomotion(dt);
		position += velocity * dt;
	}	
}

void PlayerBehaviour::updateAnimations(float dt)
{
	int newAnim = chara->animationController.getCurrentAnimID();
	switch (state)
	{
		case State::Dying:
		case State::Dead:
		{
			newAnim = ANIM_PLAYER_DIE;
			break;
		}
		case State::Playing:
		{
			newAnim = ANIM_PLAYER_IDLE;
		}
	}

	if (state == State::Playing)
	{
		if (!chara->grounded)
		{
			if (chara->velocity.y > 0)
			{
				newAnim = ANIM_PLAYER_GLIDE;
			}
			else
			{
				newAnim = ANIM_PLAYER_AIR;
			}
		}
		else
		{
			if (abs(chara->velocity.x) > 3.f)
			{
				newAnim = (abs(x) < 0.2f) ? ANIM_PLAYER_YAW : ANIM_PLAYER_WALK;
			}
		}
		
	}
	if (newAnim == chara->animationController.getCurrentAnimID())
	{
		chara->animationController.update(dt);
	}
	else
	{
		chara->animationController.change(newAnim);		
	}
}

void AIBehaviour::updateLogic(float dt, const sf::Vector2f& screenSize, sf::Vector2f& velocity, sf::Vector2f& position)
{
	if (state == State::Dying  && chara->sprite.getPosition().y > screenSize.y + chara->size.y)
	{
		changeState(State::Dead);
	}
	else if (state == State::Falling)
	{
		fallTime += dt;
		if (collisionsElapsed >= 0)
		{
			collisionsElapsed -= dt;
			if (collisionsElapsed < 0)
			{
				collisionsDisabled = false;
			}
		}

		velocity.y = 100.0f;
		velocity.x = 20.f * sin(3*fallTime);
		position.x = fallStartPosition.x + velocity.x;
		position.y += velocity.y * dt;
	}
	else if (state == State::Inflating)
	{
		if (chara->animationController.getCurrentAnimID() == ANIM_ENEMY_INFLATE && chara->animationController.isFinished())
		{
			changeState(State::Playing);
		}
	}
	else
	{
		velocity = chara->defaultLocomotion(dt);
		position += velocity * dt;
	}	
}

void AIBehaviour::updateAnimations(float dt)
{
	int newAnim = chara->animationController.getCurrentAnimID();
	switch (state)
	{
		case State::Dying:
		case State::Dead:
		{
			newAnim = ANIM_ENEMY_DIE;
			break;
		}
		case State::Falling:
		{
			newAnim = ANIM_ENEMY_FALL;
			break;
		}
		case State::Inflating:
		{
			newAnim = ANIM_ENEMY_INFLATE;
			break;
		}
		case State::Playing:
		{
			newAnim = ANIM_ENEMY_IDLE;
			break;
		}
	}

	if (state == State::Playing)
	{
		if (!chara->grounded)
		{
			if (chara->velocity.y > 0)
			{
				newAnim = ANIM_ENEMY_GLIDE;
			}
			else
			{
				newAnim = ANIM_ENEMY_AIR;
			}
		}
	}
	if (newAnim == chara->animationController.getCurrentAnimID())
	{
		chara->animationController.update(dt);
	}
	else
	{
		chara->animationController.change(newAnim);
	}
}