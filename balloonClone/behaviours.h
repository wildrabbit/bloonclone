#ifndef BEHAVIOUR_H
#define BEHAVIOUR_H

#include <SFML\Graphics.hpp>

struct Character;
struct Input;

const int ANIM_PLAYER_IDLE = 0;
const int ANIM_PLAYER_WALK = 1;
const int ANIM_PLAYER_YAW = 2;
const int ANIM_PLAYER_AIR = 3;
const int ANIM_PLAYER_GLIDE = 4;
const int ANIM_PLAYER_DIE = 5;

const int ANIM_ENEMY_IDLE = 0;
const int ANIM_ENEMY_GLIDE = 1;
const int ANIM_ENEMY_AIR = 2;
const int ANIM_ENEMY_DIE = 3;
const int ANIM_ENEMY_FALL = 4;
const int ANIM_ENEMY_INFLATE = 5;

struct CharaBehaviour
{
	Character* chara = nullptr;
	float x = 0.0f;
	bool jumpRequest = false;

	CharaBehaviour(Character* chara) : chara(chara){}
	virtual ~CharaBehaviour(){ chara = nullptr; }

	virtual void resolveInput(float dt) = 0;
	virtual void updateLogic(float dt, const sf::Vector2f& screenSize, sf::Vector2f& velocity, sf::Vector2f& position) = 0;
	virtual bool alive() = 0;
	virtual void updateAnimations(float dt) = 0;
};

struct PlayerBehaviour : public CharaBehaviour
{
	enum class State
	{
		Playing,
		Dying,
		Dead
	};
	State state;
	Input* input;

	float deadElapsed;
	const float deadRecoveryDelay = 2.0f;

	const int maxLives = 3;
	int lives;

	const int maxHP = 2;
	int HP;

	float disableInputElapsed = -1.f;
	const float disableInputDelay = 0.4f;

	PlayerBehaviour(Character* chara, Input* input);
	~PlayerBehaviour(){}

	void resolveInput(float dt) override;
	void changeState(State nextState, bool force = false);
	bool alive() override;
	void updateLogic(float dt, const sf::Vector2f& screenSize, sf::Vector2f& velocity, sf::Vector2f& position) override;
	void updateAnimations(float dt) override;
};

struct AIBehaviour : public CharaBehaviour
{
	enum class State
	{
		Inflating,
		Playing,
		Falling,
		Dying,
		Dead
	};

	bool collisionsDisabled = false;
	float collisionsElapsed = -1;
	const float collisionGraceDelay = 0.3f;

	State state;
	const float jumpDecisionDelay = 0.2f;
	float jumpDelayElapsed = -1.f;

	const float moveDecisionDelay = 3.0f;
	const float moveXChance = 0.64f;
	float moveElapsed = -1.f;

	sf::Vector2f fallStartPosition;
	float fallTime;

	AIBehaviour(Character* chara) : CharaBehaviour(chara), state(State::Playing){}
	~AIBehaviour(){}
	void resolveInput(float dt) override;
	void changeState(State nextState, bool force = false);
	bool alive() override;
	void updateLogic(float dt, const sf::Vector2f& screenSize, sf::Vector2f& velocity, sf::Vector2f& position) override;
	void updateAnimations(float dt) override;
};
#endif