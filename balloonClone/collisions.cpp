#include "collisions.h"

bool collisionAABBAABB(const AABB& box1, const AABB& box2, float& minAxisProjection, Axis& axis)
{
	// Use SAT
	// First, X axis

	minAxisProjection = FLT_MAX;
	axis = Axis::None;

	sf::Vector2f projX1 = { box1.center.x, box1.halfSize.x };
	sf::Vector2f projX2 = { box2.center.x, box2.halfSize.x };
	float centerDistance = fabs(box1.center.x - box2.center.x);
	float sumSizes = box1.halfSize.x + box2.halfSize.x;
	float proj = centerDistance - sumSizes;
	if (proj >= 0.0f)
	{
		axis = Axis::None;
		minAxisProjection = 0.0f;
		return false;
	}
	else
	{
		axis = Axis::x;
		minAxisProjection = std::min(minAxisProjection, fabs(proj));
	}

	// Y axis
	centerDistance = fabs(box1.center.y - box2.center.y);
	sumSizes = box1.halfSize.y + box2.halfSize.y;
	proj = centerDistance - sumSizes;
	float aProj = fabs(proj);
	if (proj >= 0.0f)
	{
		axis = Axis::None;
		minAxisProjection = 0.0f;
		return false;
	}
	else if (aProj < minAxisProjection)
	{
		axis = Axis::y;
		minAxisProjection = aProj;
	}
	return true;
}

bool collisionCircleCircle(const Circle& c1, const Circle& c2, sf::Vector2f& projection)
{
	projection = { c2.center.x - c1.center.x, c2.center.y - c1.center.y };
	return (c1.radius + c2.radius)*(c1.radius + c2.radius) < projection.x*projection.x + projection.y*projection.y;
}

bool collisionAABBCircle(const AABB& box, const Circle& c, sf::Vector2f& projection)
{
	return false;
}