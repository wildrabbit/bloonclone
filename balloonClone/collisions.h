#ifndef COLLISIONS_H
#define COLLISIONS_H

#include <SFML/Graphics.hpp>

struct Circle
{
	sf::Vector2f center;
	float radius;
};

enum class Axis
{
	None,
	x,
	y
};


struct AABB
{
	sf::Vector2f center;
	sf::Vector2f halfSize;
	int mask;
	int id;
};

bool collisionAABBAABB(const AABB& box1, const AABB& box2, float& minAxisProjection, Axis& axis);
bool collisionCircleCircle(const Circle& c1, const Circle& c2, sf::Vector2f& projection);
bool collisionAABBCircle(const AABB& box, const Circle& c, sf::Vector2f& projection);

#endif