#include "entity.h"
#include "behaviours.h"

static const float FLY_IMPULSE = -250;
static const float MAX_HORIZ_SPEED = 220.0f;
static const float MAX_FALL_SPEED = 400.0f;
static const float GRAVITY = 980.f;
static const float DRAG = 0.88f;

static const float SP_WIDTH = 32.0f;
static const float SP_HEIGHT = 64.0f;

static const float DEAD_ZONE = 0.25f;

static const float AXIS_SCALE = 100.0f;


int sdbm(const char* string)
{
	int hash = 0;
	int chara;
	while (chara = *string++)
	{
		hash = chara + (hash << 6) + (hash << 16) - hash;
	}
	return hash;
}

void Entity::initAABB(int mask)
{
	aabb.id = id;
	aabb.center = sprite.getPosition() - sprite.getOrigin() + size * 0.5f;
	aabb.halfSize = size * 0.5f;
	aabb.mask = mask;
}

void Block::init(const sf::Vector2f& pos, const sf::Vector2f& size, const sf::Color& color, const std::string& name, int mask)
{
	this->name = name;
	id = sdbm(name.c_str());
	this->size = size;
	texture.loadFromFile("ground.png");
	texture.setRepeated(true);
	sprite = sf::Sprite(texture);
	sprite.setTextureRect({ 0, 0, (int)size.x, (int)size.y });
	sprite.setOrigin(size * 0.5f);
	sprite.setPosition(pos);

	initAABB(mask);
}


bool Character::isSplit()
{
	return wrapAABB.halfSize.x != 0 && wrapAABB.halfSize.y != 0;
}

void Character::initAABB(int mask)
{
	Entity::initAABB(mask);

	wrapAABB.id = id;
	wrapAABB.mask = mask;
	wrapAABB.halfSize = { aabb.halfSize.x, aabb.halfSize.y };
	wrapAABB.center = aabb.center;
}

void Character::updateAABB(const sf::Vector2f& screenSize)
{
	aabb.center = sprite.getPosition() - sprite.getOrigin() + sf::Vector2f(size.x / 2, size.y / 2);
	aabb.halfSize = size * 0.5f;

	wrapAABB.halfSize = { 0.0f, 0.0f };
	wrapAABB.center = aabb.center;

	if (aabb.center.x < aabb.halfSize.x)
	{
		float deltaX = aabb.halfSize.x - aabb.center.x;
		wrapAABB.halfSize = { deltaX / 2, aabb.halfSize.y };
		wrapAABB.center.x = screenSize.x - wrapAABB.halfSize.x;
	}
	else if (aabb.center.x > screenSize.x - aabb.halfSize.x)
	{
		float deltaX = aabb.center.x + aabb.halfSize.x - screenSize.x;
		wrapAABB.halfSize = { deltaX / 2, aabb.halfSize.y };
		wrapAABB.center.x = wrapAABB.halfSize.x;
	}
}

void Character::setBehaviour(CharaBehaviour* newBehaviour)
{
	if (behaviour != nullptr && behaviour != newBehaviour)
	{
		delete behaviour;
		behaviour = nullptr;
	}
	behaviour = newBehaviour;
}

void Character::update(const sf::Vector2f& screenSize, float dt)
 {
	if (behaviour == nullptr) return;

	behaviour->resolveInput(dt);
	
	sf::Vector2f auxVelocity = velocity;
	sf::Vector2f oldPos = sprite.getPosition();
	sf::Vector2f newPos = oldPos;// +auxVelocity * dt;

	behaviour->updateLogic(dt, screenSize, auxVelocity, newPos);

	velocity = auxVelocity;
	oldPos = sprite.getPosition();
	sprite.setPosition(newPos);
	updateAABB(screenSize);

	behaviour->updateAnimations(dt);

	sf::Vector2f scale = sprite.getScale();
	if (velocity.x > 0)
	{
		sprite.setScale({ abs(scale.x), scale.y });
	}
	else if (velocity.x < 0)
	{
		sprite.setScale({ -abs(scale.x), scale.y });
	}
}

void Character::init(const sf::Vector2f& pos, const sf::Vector2f& velocity, const sf::Vector2f& size, const sf::Color& color, const AnimData& data, TextureManager* textManager, const std::string& name, int mask)
{
	this->name = name;
	id = sdbm(name.c_str());
	this->size = size;
	sprite = sf::Sprite();
	sprite.setOrigin(size.x * 0.5f, size.y);
	startPosition = pos;
	sprite.setPosition(startPosition);
	this->velocity = velocity;
	oldPos = pos;

	grounded = false;
	flying = velocity.y > 0;
	initAABB(mask);

	// animations
	animationController.init(&sprite, textManager);
	for (size_t i = 0; i < data.entries.size(); ++i)
	{
		AnimEntry entry;
		entry.id = data.ids[i];
		entry.looping = data.entries[i].looping;
		entry.numFrames = data.entries[i].frames.size();
		entry.frames.resize(data.entries[i].frames.size());
		std::copy(data.entries[i].frames.begin(), data.entries[i].frames.end(), entry.frames.begin());
		
		animationController.add(data.ids[i], entry);
	}
	animationController.change(data.startAnim);
}

bool Character::alive()
{
	return behaviour->alive();
}

sf::Vector2f Character::defaultLocomotion(float dt)
{
	sf::Vector2f auxVelocity = velocity;
	float xInput = behaviour->x;
	float jumpRequested = behaviour->jumpRequest;
	if (xInput > DEAD_ZONE && auxVelocity.x < 0 || xInput < -DEAD_ZONE && auxVelocity.x > 0)
	{
		auxVelocity.x = xInput * acceleration * dt;
	}
	else auxVelocity.x += xInput * acceleration * dt;
	
	if (abs(xInput) < DEAD_ZONE)
	{
		if (abs(auxVelocity.x) > 0.01f)
		{
			auxVelocity.x *= DRAG;
			if (abs(velocity.x) < 0.001f)
			{
				auxVelocity.x = 0.0f;
			}
		}
	}

	if (abs(auxVelocity.x) > MAX_HORIZ_SPEED)
	{
		auxVelocity.x = MAX_HORIZ_SPEED * ((signbit(auxVelocity.x)) ? -1 : 1);
	}

	if (jumpRequested)
	{
		auxVelocity.y = FLY_IMPULSE;
	}
	else
	{
		if (grounded)
		{
			auxVelocity.y = 0;
		}
	}
	auxVelocity.y += GRAVITY * dt;
	if (auxVelocity.y > MAX_FALL_SPEED)
	{
		auxVelocity.y = MAX_FALL_SPEED;
	}
	return auxVelocity;
}