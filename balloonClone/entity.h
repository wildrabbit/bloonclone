#ifndef ENTITY_H
#define ENTITY_H

#include <string>
#include <functional>
#include <SFML\Graphics.hpp>
#include "collisions.h"
#include "animationController.h"

struct Entity
{
	std::string name;
	int id;
	sf::Texture texture;
	sf::Sprite sprite;
	sf::Vector2f size;
	AABB aabb;
	float friction;
	float bounce;

	virtual void initAABB(int mask);
};

struct Block : public Entity
{
	void init(const sf::Vector2f& pos, const sf::Vector2f& size, const sf::Color& color, const std::string& name = "", int mask = 0);
};


enum CharacterAnim
{
	Idle,
	Move,
	Yaw
};

struct CharaBehaviour;
struct GameData;
struct Character : public Entity
{
	CharaBehaviour* behaviour = nullptr;
	void setBehaviour(CharaBehaviour* behaviour);

	bool animated = false;

	CharacterAnim animType;
	sf::Texture walkTexture;
	sf::Texture yawTexture;
	sf::Vector2f startPosition;

	float acceleration;
	sf::Vector2f velocity;
	bool grounded = false;
	bool flying = false;
	AABB wrapAABB;
	sf::Vector2f oldPos;

	bool isSplit();
	bool alive();

	int curFrame = 0;
	int numFrames = 1;
	float frameDelay = 0.1f;
	float frameElapsed = -1.f;

	void initAABB(int mask) override;
	void updateAABB(const sf::Vector2f& screenSize);

	void update(const sf::Vector2f& screenSize, float dt);
	void init(const sf::Vector2f& pos, const sf::Vector2f& velocity, const sf::Vector2f& size, const sf::Color& color, const AnimData& data, TextureManager* textManager, const std::string& name = "", int mask = 0);
	
	AnimationController animationController;

	sf::Vector2f defaultLocomotion(float dt);
};
#endif