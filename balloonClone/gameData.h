#ifndef GAMEDATA_H
#define GAMEDATA_H

#include <SFML\Graphics.hpp>
#include "input.h"
#include "entity.h"

struct GameData
{
	sf::RenderWindow window;
	sf::Vector2f windowSize;
	sf::View view;
	Input input;

	sf::Color bgColor;
	Character player;
	Character enemy1;
	Character enemy2;

	std::map<int, Entity*> entities;

	sf::View left;
	sf::View right;
	std::vector<Character*> leftSide;
	std::vector<Character*> rightSide;

	std::vector<Block> blocks;
};

#endif