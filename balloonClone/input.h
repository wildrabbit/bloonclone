#ifndef INPUT_H
#define INPUT_H

struct Input
{
	enum ButtonFlag
	{
		A = 1,
		B = 2,
		C = 3,
		D = 4,
		LB = 5,
		RB = 6,
		Select = 7,
		Start = 8,
		LStick = 9,
		RStick = 10
	};
	float xAxis = 0.f;
	float yAxis = 0.f;
	float zAxis = 0.f;

	bool jumpPressed = false;
	bool jumpJustPressed = false;
};

#endif