#include <SFML/Graphics.hpp>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <vector>

#include "gameData.h"
#include "collisions.h"
#include "behaviours.h"
#include "textureManager.h"

//---------------


const int TEXTURE_GROUND = 0;
const int TEXTURE_PLAYER_IDLE = 1;
const int TEXTURE_PLAYER_WALK = 2;
const int TEXTURE_PLAYER_YAW = 3;
const int TEXTURE_PLAYER_AIR = 4;
const int TEXTURE_PLAYER_DIE = 5;

const int TEXTURE_ENEMY_IDLE = 6;
const int TEXTURE_ENEMY_AIR = 7;
const int TEXTURE_ENEMY_FALL = 8;
const int TEXTURE_ENEMY_DIE = 9;
const int TEXTURE_ENEMY_INFLATE = 10;

const int NUM_TEXTURES = 11;

const std::string TEXTURE_FILENAMES[] = {
	"ground.png",
	"chara.png",
	"chara_walk.png",
	"chara_yaw.png",
	"chara_air.png",
	"chara_die.png",
	"enemy.png",
	"enemy_air.png",
	"enemy_fall.png",
	"enemy_die.png",
	"enemy_inflate.png"
};
//---------------


const int WINDOW_WIDTH = 640;
const int WINDOW_HEIGHT = 480;

const float GROUND_HEIGHT = 400;

const float SP_WIDTH = 32.0f;
const float SP_HEIGHT = 64.0f;

const float AXIS_SCALE = 100.0f;

const float TIME_DELTA_EPSILON = 0.025F;

// Declare and load a font
sf::Font font;
sf::Text dbg1;
sf::Text dbg2;


enum CollisionFlags : int
{
	None = 0x0,
	Player = 0x1,
	Enemy = 0x2,
	Bounds = 0x4,
	Ground = 0x8,
	Water = 0x10,
	Spark = 0x20,
	Flipper = 0x40
};


void logVector(std::ostream& str, const sf::Vector2f& v, const std::string& tag = "")
{
	str << tag << "(" << v.x << "," << v.y << ")";
}

void checkOffBounds(Character* chara, std::vector<Character*>& vLeft, std::vector<Character*>& vRight)
{
	sf::Vector2f newPos = chara->sprite.getPosition();
	sf::Vector2f& velocity = chara->velocity;
	if (newPos.x <= -1)
	{
		newPos.x = (float)WINDOW_WIDTH + ((int)newPos.x % WINDOW_WIDTH);
	}
	else if (newPos.x >= (float)WINDOW_WIDTH)
	{
		newPos.x = (float)((int)newPos.x % WINDOW_WIDTH);
	}

	if (newPos.y < chara->size.y)
	{
		newPos.y = chara->size.y;
		velocity.y = 0.0f;
	}

	if (newPos != chara->sprite.getPosition())
	{
		chara->sprite.setPosition(newPos.x, newPos.y);
	}


	float charaX = chara->sprite.getPosition().x;
	if (charaX  < chara->size.x / 2)
	{
		vLeft.push_back(chara);
	}
	else if (charaX > WINDOW_WIDTH - chara->size.x / 2)
	{
		vRight.push_back(chara);
	}
}

void separateCharacters(Character* c1, AABB* box1, Character* c2, AABB* box2, const sf::Vector2f windowSize, float proj, Axis axis)
{
	sf::Vector2f p1 = c1->sprite.getPosition();
	sf::Vector2f p2 = c2->sprite.getPosition();

	if (axis == Axis::x)
	{
		if (box1->center.x < box2->center.x)
		{
			p1.x -= proj / 2;
			p2.x += proj / 2;
		}
		else
		{
			p2.x -= proj / 2;
			p1.x += proj / 2;
		}
	}
	else if (axis == Axis::y)
	{
		if (box1->center.y < box2->center.y)
		{
			p1.y -= proj / 2;
			p2.y += proj / 2;
		}
		else
		{
			p2.y -= proj / 2;
			p1.y += proj / 2;
		}
		if (p1.y < c1->size.y)
		{
			p2.y += (c1->size.y - p1.y);
			p1.y = c1->size.y;
		}
		else if (p2.y < c2->size.y)
		{
			p1.y += (c2->size.y - p2.y);
			p2.y = c2->size.y;
		}
	}
	c1->sprite.setPosition(p1);
	c1->updateAABB(windowSize);
	c2->sprite.setPosition(p2);
	c2->updateAABB(windowSize);

}

void solvePlayerEnemyCollision(GameData& data, AABB* playerBox, AABB* enemyBox, float proj, Axis axis)
{
	//Eventually we'll have to refactor this so we can lookup any kind of entity.
	Character* c1 = dynamic_cast<Character*>(data.entities[playerBox->id]);
	Character* c2 = dynamic_cast<Character*>(data.entities[enemyBox->id]);

	if (((AIBehaviour*)c2->behaviour)->collisionsDisabled) return;

	PlayerBehaviour* playerB = dynamic_cast<PlayerBehaviour*>(c1->behaviour);
	AIBehaviour* enemyB = dynamic_cast<AIBehaviour*>(c2->behaviour);

	bool willSeparate = true;
	AIBehaviour::State newEnemyState = enemyB->state;
	PlayerBehaviour::State newPlayerState = playerB->state;

	float playerBase = playerBox->center.y + playerBox->halfSize.y;
	float enemyBase = enemyBox->center.y + enemyBox->halfSize.y;
	float heightDelta = playerBase - enemyBase;
	if (heightDelta < -SP_HEIGHT/2) // Player on top: Enemy gets hit
	{
		if (enemyB->state == AIBehaviour::State::Playing)
		{
			newEnemyState = AIBehaviour::State::Falling;
		}
		else if (enemyB->state == AIBehaviour::State::Falling || enemyB->state == AIBehaviour::State::Inflating)
		{
			newEnemyState = AIBehaviour::State::Dying;
			willSeparate = false;
		}
	}
	else if (heightDelta > SP_HEIGHT/2) // Enemy on top: Decrease player HP
	{
		if (enemyB->state == AIBehaviour::State::Playing &&  playerB->state == PlayerBehaviour::State::Playing)
		{
			//playerB->HP--;
			//if (playerB->HP == 0)
			{
				newPlayerState = PlayerBehaviour::State::Dying;
				willSeparate = false;
			}
		}
	}
	else
	{
		if (enemyB->state == AIBehaviour::State::Inflating)
		{
			willSeparate = false;
			newEnemyState = AIBehaviour::State::Dying;
		}
	}

	if (enemyB->state != newEnemyState)
	{
		enemyB->changeState(newEnemyState);
	}
	if (playerB->state != newPlayerState)
	{
		playerB->changeState(newPlayerState);
	}

	if (willSeparate)
	{
		if (axis == Axis::x)
		{
			c1->velocity.x = -c1->velocity.x;
			playerB->x = 0;
			playerB->disableInputElapsed = playerB->disableInputDelay;
			c2->velocity.x = -c2->velocity.x;
			AIBehaviour* b2= ((AIBehaviour*)c2->behaviour);
			b2->x = 0;

		}
		else
		{
			c1->velocity.y = -c1->velocity.y;
			c2->velocity.y = -c2->velocity.y;
		}
		separateCharacters(c1, playerBox, c2, enemyBox, data.windowSize, proj, axis);
	}
}

void solveEnemyEnemyCollision(GameData& data, AABB* box1, AABB* box2, float proj, Axis axis)
{
	//Eventually we'll have to refactor this so we can lookup any kind of entity.
	Character* c1 = dynamic_cast<Character*>(data.entities[box1->id]);
	Character* c2 = dynamic_cast<Character*>(data.entities[box2->id]);
	

	if (((AIBehaviour*)c1->behaviour)->collisionsDisabled  || ((AIBehaviour*)c2->behaviour)->collisionsDisabled) return;

	if (axis == Axis::x)
	{
		c1->velocity.x = -c1->velocity.x;
		AIBehaviour* b1 = ((AIBehaviour*)c1->behaviour);
		b1->x = 0;
		c2->velocity.x = -c2->velocity.x;
		AIBehaviour* b2= ((AIBehaviour*)c2->behaviour);
		b2->x = 0;

	}
	else
	{
		c1->velocity.y = -c1->velocity.y;
		c2->velocity.y = -c2->velocity.y;
	}

	separateCharacters(c1, box1, c2, box2, data.windowSize, proj, axis);
}

void solveCharacterGroundCollision(GameData& data, AABB* playerBox, AABB* groundBox, float proj, Axis axis)
{
	//Eventually we'll have to refactor this so we can lookup any kind of entity.
	Character* c1 = dynamic_cast<Character*>(data.entities[playerBox->id]);
	sf::Vector2f p1 = c1->sprite.getPosition();
	
	if (axis == Axis::x)
	{
		if (playerBox->center.x < groundBox->center.x)
		{
			p1.x -= proj;
		}
		else
		{
			p1.x += proj;
		}
	}
	else if (axis == Axis::y)
	{
		if (playerBox->center.y < groundBox->center.y)
		{
			p1.y -= proj;		
			c1->grounded = true;
			AIBehaviour* ai = dynamic_cast<AIBehaviour*>(c1->behaviour);
			if (ai != nullptr && ai->state == AIBehaviour::State::Falling)
			{
				ai->changeState(AIBehaviour::State::Inflating);
			}
		}
		else
		{
			p1.y += proj;
			c1->velocity.y = 0;
		}

/*		if (p1.y > GROUND_HEIGHT)
		{
			p1.y -= p1.y - GROUND_HEIGHT;
			c1->grounded = true;
		}
		else */if (p1.y < c1->size.y)
		{
			p1.y = c1->size.y;
		}
	}
	sf::Vector2f old = c1->sprite.getPosition();
	c1->sprite.setPosition(p1);
	c1->updateAABB(data.windowSize);
}

void solveCollisions(GameData& data)
{
	float proj = 0.0f;
	Axis axis = Axis::None;

	typedef std::vector<AABB*>::iterator AABBListIter;
	std::vector<AABB*> dynamicCollisionList;
	if (data.player.alive() && ((PlayerBehaviour*)data.player.behaviour)->state == PlayerBehaviour::State::Playing)
	{
		dynamicCollisionList.push_back(&data.player.aabb);
		if (data.player.isSplit())
		{
			dynamicCollisionList.push_back(&data.player.wrapAABB);
		}
	}
	if (data.enemy1.alive() && ((AIBehaviour*)data.enemy1.behaviour)->state != AIBehaviour::State::Dying)
	{
		dynamicCollisionList.push_back(&data.enemy1.aabb);
		if (data.enemy1.isSplit())
		{
			dynamicCollisionList.push_back(&data.enemy1.wrapAABB);
		}
	}
	if (data.enemy2.alive() && ((AIBehaviour*)data.enemy2.behaviour)->state != AIBehaviour::State::Dying)
	{
		dynamicCollisionList.push_back(&data.enemy2.aabb);
		if (data.enemy2.isSplit())
		{
			dynamicCollisionList.push_back(&data.enemy2.wrapAABB);
		}
	}
	
	std::sort(dynamicCollisionList.begin(), dynamicCollisionList.end(), [](const AABB* b1, const AABB* b2){ return b1->center.y + b1->halfSize.y < b2->center.y + b2->halfSize.y; });
	
	bool collisionsFound = false;
	int iters = 0;
	do
	{
		collisionsFound = false;
		// Static vs Dynamic
		for (size_t i = 0; i < data.blocks.size(); ++i)
		{
			for (AABBListIter it1 = dynamicCollisionList.begin(); it1 != dynamicCollisionList.end(); ++it1)
			{
				if (collisionAABBAABB(data.blocks[i].aabb, *(*it1), proj, axis))
				{
					solveCharacterGroundCollision(data, *it1, &data.blocks[i].aabb, proj, axis);
					collisionsFound = true;
				}
			}			
		}

		// Dynamic vs Dynamic
		for (AABBListIter it1 = dynamicCollisionList.begin(); it1 != dynamicCollisionList.end(); ++it1)
		{
			for (AABBListIter it2 = it1 + 1; it2 < dynamicCollisionList.end(); ++it2)
			{
				auto aabb1 = *it1;
				auto aabb2 = *it2;
				if (aabb1->id == aabb2->id) continue;

				if (collisionAABBAABB(*aabb1, *aabb2, proj, axis))
				{
					collisionsFound = true;
					bool isPlayer1 = (aabb1->mask & CollisionFlags::Player) != 0;
					bool isPlayer2 = (aabb2->mask & CollisionFlags::Player) != 0;
					bool isEnemy1 = (aabb1->mask & CollisionFlags::Enemy) != 0;
					bool isEnemy2 = (aabb2->mask & CollisionFlags::Enemy) != 0;

					if (isPlayer1 && isEnemy2)
					{
						solvePlayerEnemyCollision(data, aabb1, aabb2, proj, axis);
					}
					if (isPlayer2 && isEnemy1)
					{
						solvePlayerEnemyCollision(data, aabb2, aabb1, proj, axis);
					}

					if (isEnemy1 && isEnemy2)
					{
						std::cout << "Enemy-Enemy collision on loop " << iters << std::endl;
						solveEnemyEnemyCollision(data, aabb1, aabb2, proj, axis);
					}
				}

			}
		}
		iters++;
	} while (collisionsFound && iters < 5);	

	if (iters > 1)
	{
		//Calculate exit speeds!
	}
}

struct CharacterConfig
{
	sf::Vector2f pos;
	sf::Vector2f vel;
	sf::Vector2f size;
	sf::Color color;
	std::string name;
	int mask;
	bool playerControlled;
};

struct BlockConfig
{
	sf::Vector2f pos;
	sf::Vector2f size;
	sf::Color color;
	std::string name;
	int mask;
};

//const int NUM_CHARACTERS = 3;
//CharacterConfig charaConfigs[NUM_CHARACTERS] = {
//	{
//	},
//	{},
//	{},
//};

const int NUM_BLOCKS = 3;
BlockConfig blockConfigs[NUM_BLOCKS] = {
	{ { 80.f, 220.f }, { 140.f, 32.f }, sf::Color::Green, "block00", CollisionFlags::Ground }
	, { { 560.f, 280.f }, { 140.f, 32.f }, sf::Color::Green, "block01", CollisionFlags::Ground }
	//, { { 320.f, 300.f }, { 190.f, 32.f }, sf::Color::Green, "block02", CollisionFlags::Ground }
	, { { (float)WINDOW_WIDTH * 0.5f, GROUND_HEIGHT + ((float)WINDOW_HEIGHT - GROUND_HEIGHT) / 2 }, { (float)WINDOW_WIDTH, (float)WINDOW_HEIGHT - GROUND_HEIGHT }, sf::Color::Green, "ground", CollisionFlags::Ground }
};


void initScene(GameData& data, TextureManager& textureManager)
{
	for (int i = 0; i < NUM_TEXTURES; ++i)
	{
		textureManager.addTexture(i, TEXTURE_FILENAMES[i]);
	}

	data.window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "SFML");
	data.window.setFramerateLimit(60);
	sf::Vector2u size = data.window.getSize();
	data.windowSize = sf::Vector2f((float)size.x, (float)size.y);
	sf::Vector2f centre = sf::Vector2f(data.windowSize.x * 0.5f, data.windowSize.y * 0.5f);
	data.view.setCenter(centre);
	data.view.setSize(sf::Vector2f((float)WINDOW_WIDTH, (float)WINDOW_HEIGHT));
	data.window.setView(data.view);

	AnimData playerAnims = {
		{
			ANIM_PLAYER_IDLE, ANIM_PLAYER_WALK, ANIM_PLAYER_YAW, ANIM_PLAYER_AIR, ANIM_PLAYER_GLIDE, ANIM_PLAYER_DIE
		},
		{
			{ false, { { TEXTURE_PLAYER_IDLE, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f } } },
			{ 
				true,  
				{ 
					{ TEXTURE_PLAYER_WALK, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f }, 
					{ TEXTURE_PLAYER_WALK, { (int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f },
					{ TEXTURE_PLAYER_WALK, { 2 * (int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f },
					{ TEXTURE_PLAYER_WALK, { 3 * (int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f } 
				} 
			},
			{ false, { { TEXTURE_PLAYER_YAW, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f } } },
			{ true, { { TEXTURE_PLAYER_AIR, { (int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f }, { TEXTURE_PLAYER_AIR, { 2*(int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f } } },
			{ false, { { TEXTURE_PLAYER_AIR, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f } } },
			{ true, 
				{ 
					{ TEXTURE_PLAYER_DIE, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f },
					{ TEXTURE_PLAYER_DIE, { (int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f },
					{ TEXTURE_PLAYER_DIE, { 2*(int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f }
			} }
		},
		ANIM_PLAYER_IDLE
	};

	AnimData enemyAnims = {
		{
			ANIM_ENEMY_IDLE, ANIM_ENEMY_GLIDE, ANIM_ENEMY_AIR, ANIM_ENEMY_DIE, ANIM_ENEMY_FALL, ANIM_ENEMY_INFLATE
		},
		{
			{ false, { { TEXTURE_ENEMY_IDLE, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f } } },
			{ false, { { TEXTURE_ENEMY_AIR, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f } } },
			{ true, { 
				{ TEXTURE_ENEMY_AIR, { (int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f },
				{ TEXTURE_ENEMY_AIR, { 2*(int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f }
			} },
			{ true, { 
					{ TEXTURE_ENEMY_DIE, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f },
					{ TEXTURE_ENEMY_DIE, {(int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f } 
				}
			},
			{ false, { { TEXTURE_ENEMY_FALL, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.05f } } },
			{ false,
			{
				{ TEXTURE_ENEMY_INFLATE, { 0, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 5.00f },
				{ TEXTURE_ENEMY_INFLATE, { (int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.4f },
				{ TEXTURE_ENEMY_INFLATE, { 2 * (int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.4f },
				{ TEXTURE_ENEMY_INFLATE, { 3 * (int)SP_WIDTH, 0, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.4f },
				{ TEXTURE_ENEMY_INFLATE, { 0, (int)SP_HEIGHT, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.4f },
				{ TEXTURE_ENEMY_INFLATE, { (int)SP_WIDTH, (int)SP_HEIGHT, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.4f },
				{ TEXTURE_ENEMY_INFLATE, { 2 * (int)SP_WIDTH, (int)SP_HEIGHT, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.4f },
				{ TEXTURE_ENEMY_INFLATE, { 3 * (int)SP_WIDTH, (int)SP_HEIGHT, (int)SP_WIDTH, (int)SP_HEIGHT }, 0.4f }
			} }
		},
		ANIM_ENEMY_IDLE
	};
	

	data.player.init({ WINDOW_WIDTH * 0.5f, GROUND_HEIGHT }, { 0.0f, 0.0f }, { SP_WIDTH, SP_HEIGHT }, sf::Color::Magenta, playerAnims, &textureManager, "player", CollisionFlags::Player);
	PlayerBehaviour* playerB = new PlayerBehaviour(&data.player, &data.input);
	data.player.setBehaviour(playerB);
	playerB->changeState(PlayerBehaviour::State::Playing, true);
	data.entities[data.player.id] = &data.player;
	
	data.enemy1.init({ WINDOW_WIDTH * 0.5f, 200 }, { 0.0f, 0.0f }, { SP_WIDTH, SP_HEIGHT }, sf::Color::Cyan, enemyAnims, &textureManager, "enemy1", CollisionFlags::Enemy);
	data.enemy1.setBehaviour(new AIBehaviour(&data.enemy1));
	data.entities[data.enemy1.id] = &data.enemy1;

	data.enemy2.init({ 80, 80 }, { 0.0f, 0.0f }, { SP_WIDTH, SP_HEIGHT }, sf::Color::Blue, enemyAnims, &textureManager, "enemy2", CollisionFlags::Enemy);
	data.enemy2.setBehaviour(new AIBehaviour(&data.enemy2));
	data.entities[data.enemy2.id] = &data.enemy2;

	data.blocks.resize(NUM_BLOCKS);
	for (int i = 0; i < NUM_BLOCKS; ++i)
	{
		data.blocks[i].init(blockConfigs[i].pos, blockConfigs[i].size, blockConfigs[i].color, blockConfigs[i].name, blockConfigs[i].mask);
		data.entities[data.blocks[i].id] = &data.blocks[i];
	}
	
	data.bgColor = sf::Color::Black;

	float w = (float)WINDOW_WIDTH;
	float h = (float)WINDOW_HEIGHT;
	data.left = sf::View({ -w * 0.5f, h * 0.5f }, { w, h });
	data.right = sf::View({ w * 1.5f, h * 0.5f }, { w, h });

	if (font.loadFromFile("D:/projects/balloonClone/balloonClone/arial.ttf"))
	{
		// Create a text
		dbg1.setFont(font);
		dbg1.setCharacterSize(30);
		dbg1.setStyle(sf::Text::Regular);
		dbg1.setFillColor(sf::Color::White);		
		dbg1.setPosition(10, GROUND_HEIGHT + 10);
		
		dbg2.setFont(font);
		dbg2.setCharacterSize(30);
		dbg2.setStyle(sf::Text::Regular);
		dbg2.setFillColor(sf::Color::White);
		dbg2.setPosition(10, GROUND_HEIGHT + 50);
	}

	solveCollisions(data);
}

void handleEvents(GameData& data)
{
	bool oldDown = data.input.jumpPressed;

	sf::Event event;

	while (data.window.pollEvent(event))
	{
		switch (event.type)
		{
			case sf::Event::Closed:
			{
				data.window.close();
				break;
			}
			case sf::Event::Resized:
			{
				data.view.setSize((float)event.size.width, (float)event.size.height);
				break;
			}
		}
	}

	bool usedKeyboard = false;
	// Keyboard input
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		data.window.close();
		usedKeyboard = true;
	}

	data.input.xAxis = data.input.yAxis = data.input.zAxis = 0.f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		data.input.xAxis = -1.f;
		usedKeyboard = true;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		data.input.xAxis = 1.f;
		usedKeyboard = true;
	}

	data.input.jumpPressed = sf::Keyboard::isKeyPressed(sf::Keyboard::Space);
	usedKeyboard = usedKeyboard || data.input.jumpPressed;
	data.input.jumpJustPressed = data.input.jumpPressed && !oldDown;

	if (sf::Joystick::isConnected(0) && !usedKeyboard)
	{
		if (sf::Joystick::hasAxis(0, sf::Joystick::Axis::X))
		{
			data.input.xAxis = sf::Joystick::getAxisPosition(0, sf::Joystick::Axis::X) / AXIS_SCALE;
		}
		if (sf::Joystick::hasAxis(0, sf::Joystick::Axis::Y))
		{
			data.input.yAxis = sf::Joystick::getAxisPosition(0, sf::Joystick::Axis::Y) / AXIS_SCALE;
		}
		if (sf::Joystick::hasAxis(0, sf::Joystick::Axis::Z))
		{
			data.input.zAxis = sf::Joystick::getAxisPosition(0, sf::Joystick::Axis::Z) / AXIS_SCALE;
		}

		int nButtons = sf::Joystick::getButtonCount(0);
		std::vector<bool> bitset(nButtons);
		int val = 0;

		for (int i = 0; i < nButtons; ++i)
		{
			bitset[i] = sf::Joystick::isButtonPressed(0, i);
			val = val | (bitset[i] << i);
		}
		/*std::ios st(nullptr);
		st.copyfmt(std::cout);
		std::cout << "Num buttons: " << nButtons << ", State: " << std::setw(sizeof(int)) << std::setfill('0') << std::hex << val << std::endl;
		std::cout.copyfmt(st);*/
		data.input.jumpPressed = bitset[static_cast<int>(Input::ButtonFlag::A)-1];
		data.input.jumpJustPressed = data.input.jumpPressed && !oldDown;
	}
}

void renderScene(GameData& data)
{
	data.window.clear(data.bgColor);
	
	for (size_t i = 0; i < data.blocks.size(); ++i)
	{
		data.window.draw(data.blocks[i].sprite);
	}

	if (data.leftSide.size() > 0)
	{
		data.window.setView(data.left);
		for (Character* chara: data.leftSide)
		{
			data.window.draw(chara->sprite);
		}
	}
	
	if (data.rightSide.size() > 0)
	{
		data.window.setView(data.right);
		for (Character* chara : data.rightSide)
		{
			data.window.draw(chara->sprite);
		}
	}
	
	data.window.setView(data.view);
	if (data.player.alive()) data.window.draw(data.player.sprite);
	if (data.enemy1.alive()) data.window.draw(data.enemy1.sprite);
	if (data.enemy2.alive()) data.window.draw(data.enemy2.sprite);
	data.window.draw(dbg1);
	data.window.draw(dbg2);
	data.window.display();
}


void update(GameData& data, float dt)
{
	data.leftSide.clear();
	data.rightSide.clear();

	//if (data.player.alive())
	{
		data.player.update(data.windowSize, dt);
		data.player.grounded = false;
	}
	//if (data.enemy1.alive())
	{
		data.enemy1.update(data.windowSize, dt);
		data.enemy1.grounded = false;
	}
	
	//if (data.enemy2.alive())
	{
		data.enemy2.update(data.windowSize, dt);
		data.enemy2.grounded = false;
	}
	
	solveCollisions(data);

	if (data.player.alive()) checkOffBounds(&data.player, data.leftSide, data.rightSide);
	if (data.enemy1.alive()) checkOffBounds(&data.enemy1, data.leftSide, data.rightSide);
	if (data.enemy2.alive()) checkOffBounds(&data.enemy2, data.leftSide, data.rightSide);
}

int main()
{	
	TextureManager textureManager;
	GameData data;
	initScene(data, textureManager);
	sf::Clock clock;
	while (data.window.isOpen())
	{
		handleEvents(data);
		float dt = clock.restart().asSeconds();
		if (dt > TIME_DELTA_EPSILON)
		{
			dt = 0.02f;
		}
		update(data, dt);
		renderScene(data);
	}
	return 0;
}