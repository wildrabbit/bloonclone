#include "texturemANAGER.h"

TextureManager::TextureManager()
	:textures()
{}

void TextureManager::addTexture(TextureId id, const std::string& filename)
{
	if (textures.find(id) == textures.end())
	{
		sf::Texture texture;
		texture.loadFromFile(filename);
		textures[id] = texture;
	}
}

sf::Texture* TextureManager::getTexture(TextureId id)
{
	return &textures[id];
}

bool TextureManager::hasTexture(TextureId id)
{
	return textures.find(id) != textures.end();
}

