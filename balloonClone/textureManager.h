#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <SFML\Graphics.hpp>
#include <map>

typedef int TextureId;
typedef std::map<TextureId, sf::Texture> TextureTable;

class TextureManager
{
private:
	TextureTable textures;
public:
	TextureManager();
	void addTexture(TextureId id, const std::string& textureName);
	sf::Texture* getTexture(TextureId id);
	bool hasTexture(TextureId id);
};

#endif